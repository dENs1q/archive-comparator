package ru.comparator;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public abstract class ArchiveComparator {
    private ArchiveComparator() {
    }

    public static void compare(String zipPath1, String zipPath2) throws IOException {

        ZipFile zipOld = new ZipFile(zipPath1);
        ZipFile zipNew = new ZipFile(zipPath2);

        Map<String, ZipEntry> entriesOld = getFileMap(zipOld);
        Map<String, ZipEntry> entriesNew = getFileMap(zipNew);


        int maxNameSizeOld = getMaxNameLength(entriesOld.keySet());
        int maxNameSizeNew = getMaxNameLength(entriesNew.keySet());

        List<ZipEntry> newFiles;
        List<ZipEntry> updatedFiles = new ArrayList<>();
        List<ZipEntry> deletedFiles = new ArrayList<>();
        List<ZipEntry> renamedOldFiles = new ArrayList<>();
        List<ZipEntry> renamedNewFiles = new ArrayList<>();

        for (ZipEntry zipEntry1 : entriesOld.values()) {
            ZipEntry zipEntry2 = entriesNew.remove(zipEntry1.getName());

            if (zipEntry2 == null) {
                // deleted
                deletedFiles.add(zipEntry1);
                continue;
            }
            if (zipEntry1.hashCode() != zipEntry2.hashCode()
                    || zipEntry1.getSize() != zipEntry2.getSize()) {
                // file has been updated
                updatedFiles.add(zipEntry1);
            }
        }
        newFiles = new ArrayList<>(entriesNew.values());

        for (ZipEntry oldEntry : entriesOld.values()) {
            for (ZipEntry newEntry : entriesNew.values()) {
                if (oldEntry.getSize() == newEntry.getSize()
                        && !oldEntry.getName().equals(newEntry.getName())) {
                    renamedOldFiles.add(oldEntry);
                    deletedFiles.remove(oldEntry);

                    renamedNewFiles.add(newEntry);
                    newFiles.remove(newEntry);
                }
            }
        }


        printResult(deletedFiles, updatedFiles, newFiles, renamedOldFiles, renamedNewFiles, maxNameSizeOld, maxNameSizeNew);
    }

    private static Map<String, ZipEntry> getFileMap(ZipFile zipFile) {
        Map<String, ZipEntry> result = new HashMap<>();
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            String entryName = entry.getName();

            if (entryName.contains("/") && !entry.isDirectory()) {
                continue;
            }
            result.put(entryName, entry);
        }
        return result;
    }

    private static int getMaxNameLength(Collection<String> names) {
        int length = 0;
        for (String name : names) {
            if (length < name.length()) {
                length = name.length();
            }
        }
        return length;
    }

    private static void printResult(List<ZipEntry> deleted, List<ZipEntry> updated, List<ZipEntry> added,
                                    List<ZipEntry> renamedOld, List<ZipEntry> renamedNew,
                                    int oldMaxNameLength, int newMaxNameLength) throws IOException {

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < oldMaxNameLength + newMaxNameLength + 11; i++) {
            stringBuilder.append("-");
        }
        stringBuilder.append("\r\n");

        for (ZipEntry zipEntry : deleted) {
            stringBuilder.append("| - ")
                    .append(zipEntry.getName());
            for (int i = 0; i < oldMaxNameLength - zipEntry.getName().length(); i++) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(" | ");

            for (int i = 0; i < newMaxNameLength + 2; i++) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(" |");
            stringBuilder.append("\r\n");
        }

        for (ZipEntry zipEntry : updated) {
            stringBuilder.append("| * ")
                    .append(zipEntry.getName());
            for (int i = 0; i < oldMaxNameLength - zipEntry.getName().length(); i++) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(" | ");

            stringBuilder.append("* ")
                    .append(zipEntry.getName());
            for (int i = 0; i < newMaxNameLength - zipEntry.getName().length(); i++) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(" |");
            stringBuilder.append("\r\n");
        }

        for (ZipEntry zipEntry : added) {
            stringBuilder.append("| ");
            for (int i = 0; i < oldMaxNameLength + 2; i++) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(" | ");

            stringBuilder.append("+ ")
                    .append(zipEntry.getName());
            for (int i = 0; i < newMaxNameLength - zipEntry.getName().length(); i++) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(" |");
            stringBuilder.append("\r\n");
        }

        for (int i = 0; i < renamedNew.size(); i++) {
            ZipEntry zipEntryOld = renamedOld.get(i);
            ZipEntry zipEntryNew = renamedNew.get(i);


            stringBuilder.append("| ? ")
                    .append(zipEntryOld.getName());
            for (int j = 0; j < oldMaxNameLength - zipEntryOld.getName().length(); j++) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(" | ");

            stringBuilder.append("? ")
                    .append(zipEntryNew.getName());
            for (int j = 0; j < newMaxNameLength - zipEntryNew.getName().length(); j++) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(" |");
            stringBuilder.append("\r\n");
        }

        for (int i = 0; i < oldMaxNameLength + newMaxNameLength + 11; i++) {
            stringBuilder.append("-");
        }

        String result = stringBuilder.toString();
        File file = new File("result.txt");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(result.getBytes());
        fileOutputStream.close();
        System.out.println(result);
    }

}


