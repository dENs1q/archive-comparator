package ru.comparator;


import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        if (args.length == 2) {
            String zipPath1 = args[0];
            String zipPath2 = args[1];
            ArchiveComparator.compare(zipPath1, zipPath2);
        } else { // if not arg
            // first window
            JFileChooser jFileChooser1 = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("zip and jar", "zip", "jar");
            jFileChooser1.setFileFilter(filter);
            jFileChooser1.setAcceptAllFileFilterUsed(false);
            int returnVal1 = jFileChooser1.showOpenDialog(null);
            // if press open || cancel window 1
            if (returnVal1 == JFileChooser.APPROVE_OPTION) {
                // seconds window
                JFileChooser jFileChooser2 = new JFileChooser();
                jFileChooser2.setFileFilter(filter);
                jFileChooser2.setAcceptAllFileFilterUsed(false);
                int returnVal2 = jFileChooser2.showOpenDialog(null);
                // if press open || cancel window 2
                if (returnVal2 == JFileChooser.APPROVE_OPTION) {
                    File selectedFile1 = jFileChooser1.getSelectedFile();
                    File selectedFile2 = jFileChooser2.getSelectedFile();
                    String zipPath1 = selectedFile1.getAbsolutePath();
                    String zipPath2 = selectedFile2.getAbsolutePath();
                    ArchiveComparator.compare(zipPath1, zipPath2);
                } else {
                    System.out.println("Press close");
                }
            } else {
                System.out.println("Press close");
            }
        }

    }
}
